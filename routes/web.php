<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\TaskController;

Route::get('/', function () {
    return '<h2>Welcome to devcamp120</h2>';
});

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/samplejson', function () {
    return response()->json(['name' => 'samplejson','data' => 'none']);
});

Route::get('/tasks', [TaskController::class, 'index']);
Route::get('/tasks/{id}', [TaskController::class, 'show']);
