<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    //
    private $tasks = [
            ['id' => 1, 'title' => 'Task 1', 'description' => 'Description for task 1', 'completed' => false],
            ['id' => 2, 'title' => 'Task 2', 'description' => 'Description for task 2', 'completed' => true],
            ['id' => 3, 'title' => 'Task 3', 'description' => 'Description for task 3', 'completed' => false]
    ];
    public function index(){
        return response()->json( $this->tasks );
    }
    public function show($id){
        foreach ( $this->tasks as $task ){
            if( $task['id'] == $id ){
                return response()->json( $task );
            }
        }
        return response()->json( ['error'=> 'Item not found'],404);
    }
}
